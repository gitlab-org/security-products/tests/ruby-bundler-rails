class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
  end

  def list_users
    list_cmd = User.lst
    exec(list_cmd)
  end

  def safe_system_call
    system("bash", "-c", "echo", params[:argument])
  end

  def safe_system_call_without_shell_dash_c
    system("echo", "-c", params[:argument])
  end

  def any_user(pat)
    username.match(/#{pat}/)
  end

  def valid_user(username)
    user_pattern = User.pattern
    username.match(/#{user_pattern}/)
  end
end
